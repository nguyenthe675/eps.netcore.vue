﻿using AutoMapper;
using EPS.Data;
using EPS.Data.Entities;
using EPS.Service.Dtos.Common;
using EPS.Service.Helpers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPS.Service
{
    public class LookupService
    {
        private EPSBaseService _baseService;
        private EPSRepository _repository;
        private IMapper _mapper;

        public LookupService(EPSRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
            _baseService = new EPSBaseService(repository, mapper);
        }


        public async Task<List<SelectItem>> GetRoles()
        {
            return await _baseService.Filter<Role, SelectItem>().ToListAsync();
        }

        public async Task<List<SelectItem>> GetPrivileges()
        {
            return await _baseService.Filter<Privilege, SelectItem>().ToListAsync();
        }

        public async Task<List<UnitTreeDto>> GetUnitTree()
        {
            var units = await _baseService.All<Unit, UnitTreeDto>().ToListAsync();

            return BuildTree(units);
        }

        private List<UnitTreeDto> BuildTree(IEnumerable<UnitTreeDto> allUnits, int? rootUnit = null)
        {
            var tree = new List<UnitTreeDto>();

            List<UnitTreeDto> roots = new List<UnitTreeDto>();
            if (rootUnit == null)
            {
                roots = allUnits.Where(x => x.ParentId == null).ToList();
            }
            else
            {
                var rootNode = allUnits.FirstOrDefault(x => x.Id == rootUnit);
                if (rootNode != null)
                {
                    roots.Add(rootNode);
                }
            }

            foreach (var r in roots)
            {
                tree.Add(r);
                UnitTreeDto nextNode = r;
                UnitTreeDto currentNode;
                UnitTreeDto parentNode;
                int index;
                int level = 1;


                while (nextNode != null)
                {
                    currentNode = nextNode;
                    nextNode = null;

                    foreach (var child in allUnits.Where(x => x.ParentId == currentNode.Id))
                    {
                        child.Parent = currentNode;
                        currentNode.Children.Add(child);
                    }

                    if (currentNode.Children.Any())
                    {
                        nextNode = currentNode.Children[0];
                        level++;
                    }
                    else
                    {
                        while (currentNode.Parent != null)
                        {
                            parentNode = currentNode.Parent;
                            index = parentNode.Children.IndexOf(currentNode);
                            if (index < parentNode.Children.Count - 1)
                            {
                                nextNode = parentNode.Children[index + 1];
                                break;
                            }
                            else
                            {
                                currentNode = parentNode;
                                level--;
                            }
                        }
                    }
                }
            }

            return tree;
        }
    }
}
